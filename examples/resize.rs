#[cfg(all(unix, not(target_os = "redox")))]
extern crate signal_hook;
#[cfg(all(unix, not(target_os = "redox")))]
extern crate termion;

#[cfg(all(unix, not(target_os = "redox")))]
mod unix_only {
    use std::io::{self, Write};
    use termion::cursor;
    use termion::event::*;
    use termion::event_sig::EventsSig;
    use termion::input::MouseTerminal;
    use termion::raw::IntoRawMode;
    use termion::terminal_size;

    use signal_hook::consts::SIGWINCH;
    use signal_hook::iterator::Signals;

    fn square(pos_x: u16, pos_y: u16, size_x: u16, size_y: u16) -> String {
        let mut sq: String = String::new();
        sq += format!(
            "{}{border}{c: <width$}",
            cursor::Goto(pos_x, pos_y),
            border = termion::color::AnsiValue(1).bg_string(),
            c = "",
            width = size_x as usize
        )
        .as_str();
        for i in 1..size_y - 1 {
            sq += format!(
                "{}{border} {fill}{c: <width$}{border} ",
                cursor::Goto(pos_x, pos_y + i),
                fill = termion::color::AnsiValue(8).bg_string(),
                border = termion::color::AnsiValue(1).bg_string(),
                c = "",
                width = (size_x - 2) as usize
            )
            .as_str();
        }
        sq += format!(
            "{}{border}{c: <width$}",
            cursor::Goto(pos_x, pos_y + size_y - 1),
            border = termion::color::AnsiValue(1).bg_string(),
            c = "",
            width = size_x as usize
        )
        .as_str();
        sq
    }

    fn display<W: Write>(stdout: &mut W, x: u16, y: u16) {
        write!(
            stdout,
            "{}{}q to exit. Change term size to see it{}{}{}",
            termion::clear::All,
            termion::cursor::Goto(1, 1),
            termion::clear::UntilNewline,
            square(1, 2, x, y - 1),
            termion::style::Reset,
        )
        .unwrap();
        stdout.flush().unwrap();
    }
    pub fn main() {
        // let mut stdout = io::stdout().into_raw_mode().unwrap();
        let mut stdout = MouseTerminal::from(io::stdout().into_raw_mode().unwrap());
        let (x, y) = terminal_size().unwrap();
        display(&mut stdout, x, y);

        let events = EventsSig::new(io::stdin(), io::stdout(), Signals::new(&[SIGWINCH]).ok());
        for c in events {
            let evt = c.unwrap();
            match evt {
                Event::Key(Key::Char('q')) => break,
                Event::Resize(x, y) => display(&mut stdout, x, y),
                _ => (),
            }
            write!(
                stdout,
                "{}{:?}{}",
                cursor::Goto(5, 5),
                evt,
                termion::clear::UntilNewline,
            )
            .unwrap();
            stdout.flush().unwrap();
        }
    }
}

#[cfg(all(unix, not(target_os = "redox")))]
fn main() {
    unix_only::main();
}

#[cfg(target_os = "redox")]
fn main() {
    eprintln!("Not available on redox");
}
