//! User input with signal. Only on unix.

use event::Event;
use input::TermRead;
use libc::{fcntl, F_GETFL, F_SETFL, O_NONBLOCK};
use signal_hook::consts::SIGWINCH;
use signal_hook::iterator::Signals;
use std::io::{self, Read, Write};
use std::os::unix::io::{AsRawFd, RawFd};
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;

/// An iterator over input events and associated signal
pub struct EventsSig {
    rx: Receiver<Result<Event, io::Error>>,
    tx_stdin: Sender<()>,
    tx_sig: Option<Sender<()>>,
}

fn block(fd: RawFd) -> io::Result<i32> {
    let flags = get_flag(fd)?;
    set_flag(fd, flags & !O_NONBLOCK)?;
    Ok(flags)
}
fn unblock(fd: RawFd) -> io::Result<i32> {
    let flags = get_flag(fd)?;
    set_flag(fd, flags | O_NONBLOCK)?;
    Ok(flags)
}

fn get_flag(fd: RawFd) -> io::Result<i32> {
    match unsafe { fcntl(fd, F_GETFL, 0) } {
        neg if neg < 0 => Err(io::Error::last_os_error()),
        flags => Ok(flags),
    }
}

fn set_flag(fd: RawFd, flags: i32) -> io::Result<()> {
    match unsafe { fcntl(fd, F_SETFL, flags) } {
        0 => Ok(()),
        _ => Err(io::Error::last_os_error()),
    }
}

impl EventsSig {
    fn idle_time() {
        thread::sleep(std::time::Duration::from_millis(30));
    }

    fn spawn_stdin<R: AsRawFd + Read + Send + 'static, W: AsRawFd + Write + Send + 'static>(
        source: R,
        out: W,
        tx: Sender<Result<Event, io::Error>>,
        rx: Receiver<()>,
    ) {
        thread::spawn(move || {
            let raw_fd = source.as_raw_fd();
            let flags = unblock(raw_fd).unwrap();
            let out_raw_fd = out.as_raw_fd();
            let flags_out = block(out_raw_fd).unwrap();
            let mut events = source.events();
            loop {
                if let Some(c) = events.next() {
                    match c {
                        Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                            EventsSig::idle_time();
                        }
                        c => {
                            if let Err(_) = tx.send(c) {
                                break;
                            }
                        }
                    }
                }
                if let Ok(_) = rx.try_recv() {
                    break;
                }
            }
            // restore fd state
            set_flag(raw_fd, flags).unwrap();
            set_flag(out_raw_fd, flags_out).unwrap();
        });
    }

    fn spawn_sig(mut signals: Signals, tx: Sender<Result<Event, io::Error>>, rx: Receiver<()>) {
        thread::spawn(move || loop {
            for sig in signals.pending() {
                if let Err(_) = tx.send(if sig == SIGWINCH {
                    let (x, y) = crate::terminal_size().unwrap();
                    Ok(Event::Resize(x, y))
                } else {
                    Ok(Event::Signals(sig))
                }) {
                    return;
                }
            }
            if let Ok(_) = rx.try_recv() {
                break;
            }
            EventsSig::idle_time();
        });
    }

    /// Create event struc to be itered on
    pub fn new<R: AsRawFd + Read + Send + 'static, W: Write + AsRawFd + Send + 'static>(
        source: R,
        out: W,
        signals: Option<Signals>,
    ) -> Self {
        let (tx, rx) = channel();
        let (tx_stdin, rev_rx_stdin) = channel();
        EventsSig::spawn_stdin(source, out, tx.clone(), rev_rx_stdin);
        let tx_sig;
        if let Some(signal) = signals {
            let (rev_tx_sig, rev_rx_sig) = channel();
            EventsSig::spawn_sig(signal, tx, rev_rx_sig);
            tx_sig = Some(rev_tx_sig);
        } else {
            tx_sig = None;
        }
        Self {
            rx,
            tx_stdin,
            tx_sig,
        }
    }
}

impl Iterator for EventsSig {
    type Item = Result<Event, io::Error>;

    fn next(&mut self) -> Option<Result<Event, io::Error>> {
        self.rx.recv().ok()
    }
}

impl Drop for EventsSig {
    fn drop(&mut self) {
        if let Err(_) = self.tx_stdin.send(()) {}
        if let Some(tx) = &self.tx_sig {
            if let Err(_) = tx.send(()) {}
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn input_with_sig() {
        use signal_hook::consts::SIGUSR1;
        let events = EventsSig::new(io::stdin(), io::stdout(), Signals::new(&[SIGUSR1]).ok());
        signal_hook::low_level::raise(SIGUSR1).unwrap();
        for evt in events {
            match evt.unwrap() {
                Event::Signals(SIGUSR1) => break,
                x => assert!(false, "recv wrong event: {:?}", x),
            }
        }
    }
}
